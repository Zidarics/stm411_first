/*
 * analog_input.c
 *
 *  Created on: Nov 20, 2018
 *      Author: zamek
 */

#include <main.h>
#include <analog_input.h>
#include <stdbool.h>

#include <FreeRTOS.h>
#include <semphr.h>
#include <cmsis_os.h>
#include <string.h>
#include <stm32f4xx.h>

/**
 * Conversion END mask bit in task notify area
 */
#define AD_EVT_CONV_END  (1<<0)

/**
 * Conversion Start mask bit in task notify area
 */
#define AD_EVT_CONV_START (1<<1)

/**
 * Conversion Stop mask bit in task notify area
 */
#define AD_EVT_CONV_STOP (1<<2)

/**
 * All mask bits in task notify area
 */
#define AD_EVT_MASK ((AD_EVT_CONV_START) | (AD_EVT_CONV_STOP) | (AD_EVT_CONV_END))

/**
 * External address of analog device
 */
extern ADC_HandleTypeDef hadc1;

/**
 * External address of the task
 */
extern TaskHandle_t TaskADHandle;

/**
 * Moving average structure declaration
 */
typedef struct {
	ai_analog_value_t queue[ANALOG_MOVING_QUEUE_SIZE];
	uint8_t queue_index;
	ai_analog_value_t current_value;
} moving_average_t;

/**
 * Moving hysteresis structure declaration
 */
typedef struct {
	ai_analog_value_t delta;
	ai_analog_value_t hyst_max;
	ai_analog_value_t hyst_min;
} moving_hysteresis_t;

/**
 * Analog channel data declaration
 */
typedef struct {
	/**
	 * Raw values from A/D device
	 */
	ai_analog_value_t raw_values[ANALOG_CHANNELS];
	/**
	 * Normalized values after Moving Average & Moving Hysteresis
	 */
	ai_analog_value_t normalized_values[ANALOG_CHANNELS];
	/**
	 * Moving average data for each channels
	 */
	moving_average_t moving_average[ANALOG_CHANNELS];
	/**
	 * Moving hysteresis data for each channels
	 */
	moving_hysteresis_t moving_hysteresis[ANALOG_CHANNELS];
	/**
	 * Initialized flag 0 if analog channel is not initialized
	 */
	bool initialized;
	/**
	 * Semaphore for data request
	 */
	SemaphoreHandle_t semaphore;
	/**
	 * Counter for conversion error
	 */
	uint32_t ad_conversion_errors;
	/**
	 * Counter fot conversion success
	 */
	uint32_t ad_conversion_success;
	/**
	 * Counter for A/D start errors
	 */
	uint32_t ad_start_errors;
	/**
	 * Counter for A/D stop errors
	 */
	uint32_t ad_stop_errors;
	/**
	 * Flag for A/D conversion is running
	 */
	bool running;
} analog_input_t;

/**
 * Analog channel data structure
 */
static analog_input_t analog_input;

int32_t ai_init(){
	analog_input.initialized = false;
	bzero(analog_input.moving_average, sizeof(analog_input.moving_average));
	bzero(analog_input.moving_hysteresis, sizeof(analog_input.moving_hysteresis));
	for(int i=0;i<ANALOG_CHANNELS;++i)
		analog_input.moving_hysteresis[i].delta = ANALOG_HYSTERESIS_DELTA;

	analog_input.semaphore = xSemaphoreCreateBinary();
	configASSERT(analog_input.semaphore);

	analog_input.initialized = true;
	return AI_OK;
}

uint32_t ai_deinit(){
	analog_input.initialized = false;
	return AI_OK;
}

/**
 * \brief Moving average processing
 */
static void moving_average() {
	for(int i=0;i<ANALOG_CHANNELS;++i)
		analog_input.normalized_values[i]=analog_input.raw_values[i];

	//TODO
}

/**
 * \brief Moving hysteresis processing
 */
static void moving_hysteresis() {
//TODO
}

/**
 * \brief Starting a conversion with DMA
 */
static void start_conversion() {
	BaseType_t result = HAL_ADC_Start_DMA(&hadc1, analog_input.raw_values, ANALOG_CHANNELS);
	if (result != HAL_OK)
		++analog_input.ad_start_errors;
	else
		analog_input.running = true;
}

void ai_process(){
	uint32_t notified_value;
	BaseType_t result;
	ai_init(ANALOG_HYSTERESIS_DELTA);

	for(;;) {
		if (analog_input.initialized) {
			//Start conversion
			start_conversion();

			//And waits for the end or timeout
			xTaskNotifyWait(AD_EVT_MASK, AD_EVT_MASK, &notified_value, pdMS_TO_TICKS(ANALOG_NOTIFY_WAIT_MS));

			//If flag of conv stop is 1 stop the dma
			if (notified_value & AD_EVT_CONV_STOP) {
				analog_input.running = false;
				result = HAL_ADC_Stop_DMA(&hadc1);
				if (result!=HAL_OK)
					++analog_input.ad_stop_errors;

				goto wait;
			}

			//If flag of conv start start the conversion
			if (notified_value & AD_EVT_CONV_START) {
				start_conversion();
				goto wait;
			}

			//If flag of conversion end processing the new values
			if (notified_value & AD_EVT_CONV_END) {
				//Try to acquire the semaphore
				if (xSemaphoreTake(analog_input.semaphore, (TickType_t) 10) == pdTRUE) {
					++analog_input.ad_conversion_success;
					moving_average();
					moving_hysteresis();
					xSemaphoreGive(analog_input.semaphore);
				}//xSemaphoreTake(
			}

		} //analog_input.initialized

wait:
		//TODO Create all checks to here

		osDelay(ANALOG_TASK_DELAY_MS);
	} //for
}

enum AI_ERRORS ai_get_values(ai_analog_value_t *values, uint8_t size){
	configASSERT(values);
	configASSERT(size);

	if (size!=ANALOG_CHANNELS)
		return AI_BAD_PARAM;

	if (!analog_input.initialized)
		return AI_UNINITIALIZED;

	if (xSemaphoreTake(analog_input.semaphore, (TickType_t) 10) == pdTRUE) {
		memcpy(values, analog_input.normalized_values,
				sizeof(analog_input.normalized_values));
		xSemaphoreGive(analog_input.semaphore);
		return AI_OK;
	}
	return AI_SEMAPHORE_BUSY;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	++analog_input.ad_conversion_success;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xTaskNotifyFromISR(TaskADHandle, AD_EVT_CONV_END, eSetBits, &xHigherPriorityTaskWoken);
}

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc) {
	HAL_ADC_Stop_DMA(&hadc1);
	++analog_input.ad_conversion_errors;
}

/**
 * \brief CLI subcommand for moving average
 */
static const char *CMD_MOVING_AVERAGE = "ma";
/**
 * \brief CLI subcommand for moving hysteresis
 */
static const char *CMD_MOVING_HYSTERESIS = "mh";

/**
 * \brief print all parameters of an analog channel
 *
 * \param channel the channel index
 */
static void cmd_show(int channel) {
	sh_printf("channel %d.: raw:%d, normalized:%d\r\n", channel,
				analog_input.raw_values[channel], analog_input.normalized_values[channel]);
}

/**
 * \brief usage of CLI
 */
static void usage() {
	sh_printf("usage %s <ma|mh>\r\n", CMD_AD);
	sh_printf("where ma is moving average and mh is moving hysteresis\r\n");
}

/**
 * \brief Show all params of moving average
 */
static void cmd_show_moving_average() {
	sh_printf("Moving average values by channel");
	for (int i=0;i<ANALOG_CHANNELS;++i) {
		sh_printf("channel %d.: current:%d, index:%d\r\n",
				  i,  analog_input.moving_average[i].current_value);
		sh_printf("queue is:");
		for (int j=0;j<ANALOG_MOVING_QUEUE_SIZE;j++)
			sh_printf("%d ", analog_input.moving_average[i].queue[j]);

		sh_printf("\r\n");
	}
}

/**
 * \brief Show all params of moving hysteresis
 */
static void cmd_show_moving_hysteresis() {
	sh_printf("Moving hysteresis values by channel");
	for (int i=0;i<ANALOG_CHANNELS;++i) {
		sh_printf("channel %d.: hyst min:%d hyst max:%d, delta:%d\r\n",
				  i, analog_input.moving_hysteresis[i].hyst_min,
				     analog_input.moving_hysteresis[i].hyst_max,
					 analog_input.moving_hysteresis[i].delta);
	}
}

void ai_cmd_ad(int argc, char *argv[]) {
	if (argc==0) {
		for (int i=0;i<ANALOG_CHANNELS;++i)
			cmd_show(i);

		sh_printf("running:%s c.success:%d, c.errors:%d, start errors:%d, stop errors:%d \r\n",
				analog_input.running ? "true" : "false",
				analog_input.ad_conversion_success,
				analog_input.ad_conversion_errors,
				analog_input.ad_start_errors,
				analog_input.ad_stop_errors);
		return;
	}

	if (argc==1) {
		if (strcmp(CMD_MOVING_AVERAGE, argv[0])==0) {
			cmd_show_moving_average();
			return;
		}

		if (strcmp(CMD_MOVING_HYSTERESIS, argv[0])==0) {
			cmd_show_moving_hysteresis();
			return;
		}
		usage();
		return;
	}

}
