/*
 * greetings.h
 *
 *  Created on: Nov 13, 2018
 *      Author: zamek
 */

#ifndef GREETINGS_H_
#define GREETINGS_H_

#include <shell.h>

/**
 * Greeting CLI interface entry point
 *
 * \param argc number of arguments, can be 0 if the command sent only
 * \param argv arguments, can be NULL if if the command sent only
 */
void cmd_greetings(int argc, char *argv[]);

/**
 * Greeting cli definition
 */
#define GREETING_COMMANDS \
	{ .command_name ="greet", .function = cmd_greetings }


#endif /* GREETINGS_H_ */
