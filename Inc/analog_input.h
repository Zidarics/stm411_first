/*
 * analog_input.h
 *
 *  Created on: Nov 20, 2018
 *      Author: zamek
 */

#ifndef ANALOG_INPUT_H_
#define ANALOG_INPUT_H_

#include <stdint.h>
#include <shell.h>
#include <stm32f4xx.h>

/**
 * \brief return values
 */
enum AI_ERRORS {
		AI_OK=0,
		AI_BAD_PARAM=-1,
		AI_SEMAPHORE_BUSY=-2,
		AI_UNINITIALIZED=-3
};

/**
 * \brief CLI command for module
 */
#define CMD_AD "ad"

/**
 * \brief CLI Command-function assignment
 */
#define ANALOG_COMMANDS \
		{ .command_name = CMD_AD, .function = ai_cmd_ad }

/**
 * \brief Define an individual type for analog value
 */
typedef uint32_t ai_analog_value_t;

/**
 * \brief Initialize analog input
 *
 * All parameters coming from constant.
 */
int32_t ai_init();

/**
 * \brief Deinit Analog inputs
 *
 * Only set to unitiaialized the device
 */
uint32_t ai_deinit();

/**
 * \brief Entry point from FreeRTOS Task
 */
void ai_process();

/**
 * \brief Getting current normalized analog values
 *
 * \param values address of the target array, cannot be NULL
 * \param size size of the target array cannot be less or equal 0 and cannot be larger then ANALOG_CHANNELS
 * \return result of the copy, doesn't change the target values if the result is not AI_OK
 */
enum AI_ERRORS ai_get_values(ai_analog_value_t *values, uint8_t size);

/**
 * \brief Entry point of the CLI
 *
 * \param argc number of arguments, can be 0 if the command sent only
 * \param argv arguments, can be NULL if if the command sent only
 *
 */
void ai_cmd_ad(int argc, char *argv[]);

/**
 * \brief AD converter callback for conversion complete event
 *
 * \param hadc addres of the AD device
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc);

/**
 * \brief AD converter callback for some error event
 *
 * \param hadc addres of the AD device
 */
void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc);

#endif /* ANALOG_INPUT_H_ */
